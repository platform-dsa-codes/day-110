import java.util.*;

class Solution {
    public String sortVowels(String s) {
        // Define a HashSet to store the vowels
        Set<Character> vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));

        // Define a StringBuilder to construct the result
        StringBuilder result = new StringBuilder(s);

        // Define two pointers for vowels and consonants
        int vowelPointer = 0;

        // Store vowels and their positions
        List<Character> sortedVowels = new ArrayList<>();
        List<Integer> vowelPositions = new ArrayList<>();

        // Iterate through the input string
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // If the character is a vowel, store it and its position
            if (vowels.contains(c)) {
                sortedVowels.add(c);
                vowelPositions.add(i);
            }
        }

        // Sort vowels
        Collections.sort(sortedVowels);

        // Iterate through the sorted vowels and update the result string
        for (int i = 0; i < sortedVowels.size(); i++) {
            result.setCharAt(vowelPositions.get(i), sortedVowels.get(i));
        }

        return result.toString();
    }
}
