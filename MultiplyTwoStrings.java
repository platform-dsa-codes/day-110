//{ Driver Code Starts
//Initial Template for Java


import java.util.*;
import java.math.*;

class Multiply{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            String a=sc.next();
            String b=sc.next();
            Solution g=new Solution();
            System.out.println(g.multiplyStrings(a,b));
        }
    }
}
// } Driver Code Ends


class Solution {
    public String multiplyStrings(String s1, String s2) {
        // Check if one of the numbers is negative
        boolean isNegative = false;
        if (s1.charAt(0) == '-') {
            isNegative = !isNegative;
            s1 = s1.substring(1);
        }
        if (s2.charAt(0) == '-') {
            isNegative = !isNegative;
            s2 = s2.substring(1);
        }
        
        // Reverse the input strings
        StringBuilder num1 = new StringBuilder(s1).reverse();
        StringBuilder num2 = new StringBuilder(s2).reverse();
        
        // Initialize an array to store the result
        int[] result = new int[num1.length() + num2.length()];
        
        // Multiply each digit and store the result in the array
        for (int i = 0; i < num1.length(); i++) {
            for (int j = 0; j < num2.length(); j++) {
                result[i + j] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
            }
        }
        
        // Carry over any digits greater than 9
        for (int i = 0; i < result.length - 1; i++) {
            result[i + 1] += result[i] / 10;
            result[i] %= 10;
        }
        
        // Reverse the array to get the result string
        StringBuilder sb = new StringBuilder();
        for (int i = result.length - 1; i >= 0; i--) {
            sb.append(result[i]);
        }
        
        // Trim leading zeros
        while (sb.length() > 1 && sb.charAt(0) == '0') {
            sb.deleteCharAt(0);
        }
        
        // Add negative sign if necessary
        if (isNegative && !sb.toString().equals("0")) {
            sb.insert(0, '-');
        }
        
        return sb.toString();
    }
}
